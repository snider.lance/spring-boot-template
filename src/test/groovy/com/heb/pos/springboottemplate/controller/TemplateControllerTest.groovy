package com.heb.pos.springboottemplate.controller

import com.heb.pos.springboottemplate.repository.TemplateRepository
import com.heb.pos.springboottemplate.service.TemplateService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@WebMvcTest
class TemplateControllerTest extends Specification {

    @Autowired
    MockMvc mvc

    @MockBean
    TemplateService templateService

    @MockBean
    TemplateRepository templateRepository

    def "when get is performed with request body then the response has status 200"() {
        expect: "Status is 200"
        mvc.perform(get("/v1/pos/template/get_service")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"serviceName\":\"Nirvana Service\" }"))
                .andExpect(status().isOk())
                .andReturn()
                .response
    }

    def "when get is performed without request body then the response has status 400"() {
        expect: "Status is 400"
        mvc.perform(get("/v1/pos/template/get_service"))
                .andExpect(status().isBadRequest())
                .andReturn()
                .response
    }

    def "when post is performed with request body then the response has status 204"() {
        expect: "Status is 204"
        mvc.perform(post("/v1/pos/template/post_service")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"serviceName\":\"Service to test7\",  \"serviceDescription\":\"JLS7\"}"))
                .andExpect(status().isNoContent())
                .andReturn()
                .response
    }

    def "when post is performed without request body then the response has status 400"() {
        expect: "Status is 400"
        mvc.perform(post("/v1/pos/template/post_service"))
                .andExpect(status().isBadRequest())
                .andReturn()
                .response
    }

    def "when delete is performed with request body then the response has status 200"() {
        expect: "Status is 200"
        mvc.perform(delete("/v1/pos/template/delete_service")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"serviceName\":\"Service to test7\",  \"serviceDescription\":\"JLS7\"}"))
                .andExpect(status().isOk())
                .andReturn()
                .response
    }

    def "when delete is performed without request body then the response has status 400"() {
        expect: "Status is 400"
        mvc.perform(delete("/v1/pos/template/delete_service"))
                .andExpect(status().isBadRequest())
                .andReturn()
                .response
    }
}
