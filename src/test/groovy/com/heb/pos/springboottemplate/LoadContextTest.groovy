package com.heb.pos.springboottemplate

import com.heb.pos.springboottemplate.controller.TemplateController
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification

@SpringBootTest
class LoadContextTest extends Specification {

    @Autowired
    TemplateController controller

    def "when context is loaded then all expected beans are created"() {
        expect: "the TemplateController is created"
        controller
    }
}
