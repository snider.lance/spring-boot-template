package com.heb.pos.springboottemplate.controller;

import com.heb.pos.springboottemplate.model.TemplateRequestModel;
import com.heb.pos.springboottemplate.model.TemplateResponseModel;
import com.heb.pos.springboottemplate.service.TemplateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping(value = "/v1/pos/template")
@Slf4j
public class TemplateController {

    @Autowired
    TemplateService templateService;

    @GetMapping(value = "/get_service", produces = "application/json")
    public ResponseEntity<TemplateResponseModel> serviceDescription(@RequestBody TemplateRequestModel templateRequestModel) {

        log.info("[START] GET the Service description");

        TemplateResponseModel templateResponseModel;

        try {
            templateResponseModel = templateService.retrieveServiceDescription(templateRequestModel.getServiceName());
        }
        catch (NullPointerException nullPointerException) {
            log.error("Service - " + templateRequestModel.getServiceName() + " was not found in the DB");
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        log.info("[END] GET the Service description");

        return ResponseEntity.ok(templateResponseModel);
    }

    @PostMapping("/post_service")
    public ResponseEntity<String> postServiceDetails(@RequestBody TemplateRequestModel postTemplateRequestModel) {

        log.info("[START] POST the Service details");

        templateService.postServiceDetails(postTemplateRequestModel);

        log.info("[END] POST the Service details");

        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @DeleteMapping("/delete_service")
    public ResponseEntity<String> deleteServiceDetails(@RequestBody TemplateRequestModel deleteTemplateRequestModel) {

        log.info("[START] DELETE the Service details");

        templateService.deleteServiceDetails(deleteTemplateRequestModel);

        log.info("[END] DELETE the Service details");

        return ResponseEntity.ok(deleteTemplateRequestModel.serviceName + " Record Successfully Deleted");
    }
}
