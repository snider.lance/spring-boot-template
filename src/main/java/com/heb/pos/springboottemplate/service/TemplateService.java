package com.heb.pos.springboottemplate.service;

import com.heb.pos.springboottemplate.entity.TemplateData;
import com.heb.pos.springboottemplate.model.TemplateRequestModel;
import com.heb.pos.springboottemplate.model.TemplateResponseModel;
import com.heb.pos.springboottemplate.repository.TemplateRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class TemplateService {

    @Autowired
    private TemplateRepository springBootTemplateRepository;

    public TemplateResponseModel retrieveServiceDescription(String serviceName) {

        TemplateResponseModel templateResponseModel = new TemplateResponseModel();

        log.info("Service Name is - {}", serviceName);

        TemplateData templateData = springBootTemplateRepository.findDistinctTopByServiceNameEquals(serviceName);

            String description = templateData.getDescription();//.concat(" - HELLO WORLD");

            log.info("Service Description is - {}", description);

            templateResponseModel.setServiceDescription(description);

        return templateResponseModel;
    }

    public void postServiceDetails(TemplateRequestModel postTemplateRequestModel) {

        TemplateData postData = new TemplateData();

        postData.setServiceName(postTemplateRequestModel.getServiceName());
        postData.setDescription(postTemplateRequestModel.getServiceDescription());

        springBootTemplateRepository.save(postData);
    }

    public void deleteServiceDetails(TemplateRequestModel deleteTemplateRequestModel) {

        TemplateData deleteData = new TemplateData();

        deleteData.setId(deleteTemplateRequestModel.getId());
        deleteData.setServiceName(deleteTemplateRequestModel.getServiceName());
        deleteData.setDescription(deleteTemplateRequestModel.getServiceDescription());

        springBootTemplateRepository.delete(deleteData);
    }
}
