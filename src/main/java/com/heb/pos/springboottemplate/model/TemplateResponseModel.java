package com.heb.pos.springboottemplate.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TemplateResponseModel {

    public String serviceDescription;
}
