package com.heb.pos.springboottemplate.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
public class TemplateRequestModel {

    public Long  id;
    public String serviceName;
    public String serviceDescription;

}
