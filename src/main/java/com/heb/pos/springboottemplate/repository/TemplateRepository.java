package com.heb.pos.springboottemplate.repository;

import com.heb.pos.springboottemplate.entity.TemplateData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TemplateRepository extends JpaRepository<TemplateData, Long> {

  TemplateData findDistinctTopByServiceNameEquals(String serviceName);
}
