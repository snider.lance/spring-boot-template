package com.heb.pos.springboottemplate.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "SPRINGBOOTTEMPLATE_TABLE")
public class TemplateData {

  @Id
  @Column(name = "ID", unique = true)
  @SequenceGenerator(name = "SPRINGBOOTTEMPLATE_TABLE_ID_GENERATOR", 
      sequenceName = "SPRINGBOOTTEMPLATE_TABLE_ID_SEQ", allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, 
      generator = "SPRINGBOOTTEMPLATE_TABLE_ID_GENERATOR")
  private Long id;
  
  @Column(name = "SERVICE_NAME")
  private String serviceName;
  
  @Column(name = "DESCRIPTION")
  private String description;

}
