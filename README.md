Spring Boot Template Project

#To Do
* Docker Integration
* New Relic Integration
* Splunk Integration
* Data Dog Integration
* Unit Tests
* Integration Tests

#Running Docker
* docker-compose up --build
#To create an image from docker file 
* docker build --tag=spring-boot-template:latest . 
#To remove containers
* docker-compose down
#Documentation
* https://docs.docker.com/reference/
