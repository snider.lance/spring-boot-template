FROM openjdk:11.0.7
MAINTAINER snider.lance@heb.com
COPY build/libs/spring-boot-template-0.0.1-SNAPSHOT.jar spring-boot-template-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/spring-boot-template-0.0.1-SNAPSHOT.jar"]